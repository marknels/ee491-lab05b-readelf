///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 491F - Software Reverse Engineering
/// Lab 05b - Readelf
///
/// @file readelf.c
/// @version 1.0
///
/// Display information about ELF files
///
/// @author Mark Nelson <marknels@hawaii.edu>
/// @brief  Lab 05b - Readelf - EE 491F - Spr 2021
/// @date   21 Feb 2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <stdint.h>
#include <inttypes.h>


// Defines
static const char    PROGRAM_NAME[] = "readelf";
static const uint8_t ELF_VERSION    = 1;
static const size_t  OFFSET_CLASS   = 4;
static const size_t  OFFSET_TYPE    = 16;
static const int     EI_NIDENT      = 16;


// Global definitions & enumerations
enum Address_Size { ADDRESS_SIZE_UNKNOWN=-1 ,ELF32=0 ,ELF64=1 } addressSize = ADDRESS_SIZE_UNKNOWN;
enum Endian       { ENDIAN_UNKNOWN=-1       ,LITTLE  ,BIG } ;

enum Endian elfEndianess     = ENDIAN_UNKNOWN;
enum Endian programEndianess = ENDIAN_UNKNOWN;
bool isSameEndianess;  // Set to true if both the elf and this program
                       // have the same endianess
size_t   programHeaderOffset    = -1;
size_t   sectionHeaderOffset    = -1;
uint16_t numberOfProgramHeaders = 0;
uint16_t numberOfSectionHeaders = 0;

struct Program_Options {
	bool displayHeader;       // Display the ELF Header Files
} programOptions;


//////////////////////////////////////////////////////////////////////////////
// Local functions

void checkEndianess() {
	int n = 1;
	
	if( *(char *)&n == 1 ) {  // little endian if true
		programEndianess = LITTLE;
	} else {
		programEndianess = BIG;
	}
	
	if( elfEndianess == programEndianess ) {
		isSameEndianess = true;
	} else {
		isSameEndianess = false;
	}
}


// This function returns the value at the current location
// It also updates the offset based on size of the data being read 
unsigned char getUnsignedChar( void** offset ) {
	const unsigned char value = *(unsigned char*)*offset;
	
	*offset += 1;  // Increment offset by 1 byte
	
	return value;
}


uint16_t getUint16( void** offset ) {
	union {
		uint16_t      machineWord;
		unsigned char bytes[2];
	} overlay;
	
	overlay.machineWord = *(uint16_t*)*offset;
	
	if( !isSameEndianess ) {
		unsigned char temp;
		temp             = overlay.bytes[0];
		overlay.bytes[0] = overlay.bytes[1];
		overlay.bytes[1] = temp;
	}

	*offset += 2;  // Increment offset by 2 bytes

	return overlay.machineWord;
}


uint32_t getUint32( void** offset ) {
	union {
		uint32_t      machineWord;
		unsigned char bytes[4];
	} overlay;
	
	overlay.machineWord = *(uint32_t*)*offset;
	
	if( !isSameEndianess ) {
		unsigned char temp;
		temp             = overlay.bytes[0];
		overlay.bytes[0] = overlay.bytes[3];
		overlay.bytes[3] = temp;

		temp             = overlay.bytes[1];
		overlay.bytes[1] = overlay.bytes[2];
		overlay.bytes[2] = temp;
	}

	*offset += 4;  // Increment offset by 4 bytes

	return overlay.machineWord;
}


uint64_t getUint64( void** offset ) {
	union {
		uint64_t      machineWord;
		unsigned char bytes[8];
	} overlay;
	
	overlay.machineWord = *(uint64_t*)*offset;
	
	if( !isSameEndianess ) {
		unsigned char temp;
		temp             = overlay.bytes[0];
		overlay.bytes[0] = overlay.bytes[7];
		overlay.bytes[7] = temp;

		temp             = overlay.bytes[1];
		overlay.bytes[1] = overlay.bytes[6];
		overlay.bytes[6] = temp;

		temp             = overlay.bytes[2];
		overlay.bytes[2] = overlay.bytes[5];
		overlay.bytes[5] = temp;

		temp             = overlay.bytes[3];
		overlay.bytes[3] = overlay.bytes[4];
		overlay.bytes[4] = temp;
	}

	*offset += 8;  // Increment offset by 8 bytes

	return overlay.machineWord;
}


void printMagic( const void* buf ) {
	printf( "  Magic:   ");

	for( int i = 0 ; i < EI_NIDENT ; i++ ) {
		printf( "%02x ", *(unsigned char*) (buf+i) );
	}
	
	printf( "\n" );
}


void printClass( const unsigned char class ) {
	// Process Class
	const unsigned char ELFCLASS32 = 0x01;
	const unsigned char ELFCLASS64 = 0x02;

	if(      (class & ELFCLASS32) != 0 ) {
		addressSize = ELF32;
		printf( "  Class:                             ELF32\n" );
	}
	else if( (class & ELFCLASS64) != 0 ) {
		addressSize = ELF64;
		printf( "  Class:                             ELF64\n" );
	}
	else {
		addressSize = ADDRESS_SIZE_UNKNOWN;
		printf( "  Class:                             INVALID\n" );
	}
}


void printEndianess( const unsigned char inEndianess ) {
	// Process Endianness

	const unsigned char ELFDATA2LSB = 0x01;
	const unsigned char ELFDATA2MSB = 0x02;

	if(      (inEndianess & ELFDATA2LSB) != 0 ) {
		elfEndianess = LITTLE;
		printf( "  Data:                              2's complement, little endian\n" );
	}
	else if( (inEndianess & ELFDATA2MSB) != 0 ) {
		elfEndianess = BIG;
		printf( "  Data:                              2's complement, big endian\n" );
	}
	else {
		elfEndianess = ENDIAN_UNKNOWN;
		printf( "  Data:                              UNKNOWN\n" );
	}
	
	checkEndianess();
}


void printVersion( const unsigned char inElfVersion ) {
	printf( "  Version:                           %x", inElfVersion );

	if( inElfVersion == ELF_VERSION ) {
		printf( " (current)");
	}
	
	printf( "\n");	
}


void printABI( const unsigned char inABI ) {
	// Process OS/ABI
	// This list comes from elf.h
	const unsigned char ELFOSABI_NONE       = 0   ;    /* UNIX System V ABI */
	const unsigned char ELFOSABI_SYSV       = 0   ;    /* Alias.  */
	const unsigned char ELFOSABI_HPUX       = 1   ;    /* HP-UX */
	const unsigned char ELFOSABI_NETBSD     = 2   ;    /* NetBSD.  */
	const unsigned char ELFOSABI_GNU        = 3   ;    /* Object uses GNU ELF extensions.  */
	const unsigned char ELFOSABI_LINUX      = ELFOSABI_GNU; /* Compatibility alias.  */
	const unsigned char ELFOSABI_SOLARIS    = 6   ;    /* Sun Solaris.  */
	const unsigned char ELFOSABI_AIX        = 7   ;    /* IBM AIX.  */
	const unsigned char ELFOSABI_IRIX       = 8   ;    /* SGI Irix.  */
	const unsigned char ELFOSABI_FREEBSD    = 9   ;    /* FreeBSD.  */
	const unsigned char ELFOSABI_TRU64      = 10  ;    /* Compaq TRU64 UNIX.  */
	const unsigned char ELFOSABI_MODESTO    = 11  ;    /* Novell Modesto.  */
	const unsigned char ELFOSABI_OPENBSD    = 12  ;    /* OpenBSD.  */
	const unsigned char ELFOSABI_ARM_AEABI  = 64  ;    /* ARM EABI */
	const unsigned char ELFOSABI_ARM        = 97  ;    /* ARM */
	const unsigned char ELFOSABI_STANDALONE = 255 ;    /* Standalone (embedded) application */

	if(      inABI == ELFOSABI_SYSV ) {
		printf( "  OS/ABI:                            UNIX - System V\n" );
	}
	else if( inABI == ELFOSABI_HPUX ) {
		printf( "  OS/ABI:                            UNIX - HP-UX\n" );
	}
}


void printABIversion( const unsigned char inABIversion ) {
	printf( "  ABI Version:                       %x\n", inABIversion );
}


void printType( const uint16_t inType ) {
	const uint16_t ET_NONE   = 0      ;
	const uint16_t ET_REL    = 0      ;
	const uint16_t ET_EXEC   = 2      ;
	const uint16_t ET_DYN    = 3      ;
	const uint16_t ET_CORE   = 4      ;
	const uint16_t ET_LOPROC = 0xff00 ;
	const uint16_t ET_HIPROC = 0xffff ;
	
	if(      inType == ET_EXEC ) {
		printf( "  Type:                              EXEC (Executable file)\n" );
	}
	else if( inType == ET_DYN ) {
		printf( "  Type:                              DYN (Shared object file)\n" );
	}
}


void printMachine( const uint16_t inMachine ) {
	const uint16_t EM_NONE    =   0 ;
	const uint16_t EM_M32     =   1 ;
	const uint16_t EM_SPARC   =   2 ;
	const uint16_t EM_386     =   3 ;
	const uint16_t EM_68K     =   4 ;
	const uint16_t EM_88K     =   5 ;
	const uint16_t EM_860     =   7 ;
	const uint16_t EM_MIPS	  =   8 ;
	const uint16_t EM_PPC64   =  21 ;
	const uint16_t EM_S390    =  22 ;
	const uint16_t EM_ARM     =  40 ;
	const uint16_t EM_X86_64  =  62 ;
	const uint16_t EM_AARCH64 = 183 ;
	
	// printf( "Machine = [%hd]\n", inMachine );

	if(      inMachine == EM_386 ) {
		printf( "  Machine:                           Intel 80386\n" );
	}
	else if( inMachine == EM_X86_64 ) {
		printf( "  Machine:                           Advanced Micro Devices X86-64\n" );
	}
	else if( inMachine == EM_AARCH64 ) {
		printf( "  Machine:                           AArch64\n" );
	}
	else if( inMachine == EM_PPC64 ) {
		printf( "  Machine:                           PowerPC64\n" );
	}
	else if( inMachine == EM_S390 ) {
		printf( "  Machine:                           IBM S/390\n" );
	}
	else if( inMachine == EM_ARM ) {
		printf( "  Machine:                           ARM\n" );
	}
}


void printObjectVersion( const uint32_t inVersion ) {
	printf( "  Version:                           0x%"PRIx32"\n", inVersion );
}


void printEntryPointAddress( const uint64_t inEntryPointAddress ) {
	printf( "  Entry point address:               0x%"PRIx64"\n", inEntryPointAddress );
}


void printProgramHeaderOffset( const uint64_t inProgramHeaderOffset ) {
	printf( "  Start of program headers:          %"PRId64" (bytes into file)\n", inProgramHeaderOffset );
	programHeaderOffset = inProgramHeaderOffset;
}


void printSectionHeaderOffset( const uint64_t inSectionHeaderOffset ) {
	printf( "  Start of section headers:          %"PRId64" (bytes into file)\n", inSectionHeaderOffset );
	sectionHeaderOffset = inSectionHeaderOffset;
}


void printFlags( const uint32_t inFlags ) {
	printf( "  Flags:                             0x%"PRIx32"\n", inFlags );
}


void printSizeOfElfHeader( const uint16_t inHeaderSize ) {
	printf( "  Size of this header:               %"PRId16" (bytes)\n", inHeaderSize );
}


void printSizeOfProgramHeader( const uint16_t inHeaderSize ) {
	printf( "  Size of program headers:           %"PRId16" (bytes)\n", inHeaderSize );
}


void printNumberOfProgramHeaders( const uint16_t inNumberOfHeaders ) {
	printf( "  Number of program headers:         %"PRId16"\n", inNumberOfHeaders );
	numberOfProgramHeaders = inNumberOfHeaders;
}


void printSizeOfSectionHeader( const uint16_t inHeaderSize ) {
	printf( "  Size of section headers:           %"PRId16" (bytes)\n", inHeaderSize );
}


void printNumberOfSectionHeaders( const uint16_t inNumberOfHeaders ) {
	printf( "  Number of section headers:         %"PRId16"\n", inNumberOfHeaders );
	numberOfSectionHeaders = inNumberOfHeaders;
}


void printSectionTableIndex( const uint16_t inIndex ) {
	printf( "  Section header string table index: %"PRId16"\n", inIndex );
}


void printUsage() {
	printf( "Usage: readelf <option(s)> elf-file(s)\n" );
	printf( " Display information about the contents of ELF format files\n" );
	printf( " Options are:\n" );
	printf( "  -h --file-header       Display the ELF file header\n" );
}


void processFile( const char* filename ) {
	printf( "ELF Header:\n" );
	
	int file = open( filename, O_RDONLY );
	if ( file == -1 ) {
		printf( "%s: %s: Error: No such file\n", PROGRAM_NAME, filename );
		return;
	}
	
	struct stat fileStatus;

	if ( fstat( file, &fileStatus ) != 0 ) {
		printf( "%s: %s: Error: Unable to get file size\n", PROGRAM_NAME, filename );
		return;
	}
	
	void* buf = mmap( NULL, fileStatus.st_size, PROT_READ, MAP_PRIVATE, file, 0 );
	if( buf == MAP_FAILED ) {
		printf( "%s: %s: Error: Unable to map file\n", PROGRAM_NAME, filename );
		return;		
	}

	// Initialize variables that change from file-to-file (when 
	// processing multiple files)	
	elfEndianess           = ENDIAN_UNKNOWN;
	addressSize            = ADDRESS_SIZE_UNKNOWN;
	programHeaderOffset    = -1;
	sectionHeaderOffset    = -1;
	numberOfProgramHeaders = 0;
	numberOfSectionHeaders = 0;

	// This points to the current location in the file
	void* offset = buf + OFFSET_CLASS;
	
	// @TODO Change all of the prints to process... 
	printMagic                 ( buf );
	printClass                 ( getUnsignedChar( &offset ));
	printEndianess             ( getUnsignedChar( &offset ));
	printVersion               ( getUnsignedChar( &offset ));
	printABI                   ( getUnsignedChar( &offset ));
	printABIversion            ( getUnsignedChar( &offset ));
	offset += 7;   // Skip the rest of the padding
	printType                  ( getUint16 ( &offset ));
	printMachine               ( getUint16 ( &offset ));
	printObjectVersion         ( getUint32 ( &offset ));
	printEntryPointAddress     ( (addressSize == ELF32) ? (uint64_t) getUint32( &offset ) : getUint64 ( &offset ) );
	printProgramHeaderOffset   ( (addressSize == ELF32) ? (uint64_t) getUint32( &offset ) : getUint64 ( &offset ) );
	printSectionHeaderOffset   ( (addressSize == ELF32) ? (uint64_t) getUint32( &offset ) : getUint64 ( &offset ) );
	printFlags                 ( getUint32 ( &offset ));
	printSizeOfElfHeader       ( getUint16 ( &offset ));
	printSizeOfProgramHeader   ( getUint16 ( &offset ));
	printNumberOfProgramHeaders( getUint16 ( &offset ));
	printSizeOfSectionHeader   ( getUint16 ( &offset ));
	printNumberOfSectionHeaders( getUint16 ( &offset ));
	printSectionTableIndex     ( getUint16 ( &offset ));


	munmap( buf, fileStatus.st_size );
	close( file );
}


int main(int argc, char* argv[]) {
	memset ( &programOptions, 0, sizeof( programOptions ));
	
	char c;

	while ((c = getopt (argc, argv, "h")) != -1) {
		switch (c) {
			case 'h':
				programOptions.displayHeader = true;
				break;
			case '?':
				fprintf (stderr, "Unknown option character '\\x%x'.\n", optopt);
				printUsage();
				exit( EXIT_FAILURE );
				break;
		}
	}

	// At this point, we've processed the switches and it's time to process the
	// filenames.  Fortunately, readelf does not process from stdin.  
	//
	// The program can have 1 or more filenames as input.  If there's only one
	// file, then the filename is not printed.  If there are multiple files, then
	// each "File: %s" is printed before processing.
	//
	// At this point, optind -- the Option Index -- is pointing to the first filename
	// and argc -- the Argument Count -- is just past the end.
	//   - If you have no filenames, they will be the same.
	//   - If you have 1 filename, optind will be one less than argc.
	//   - If you have multiple filenames, optind and argc will be separated by 2 or more.
	//
	
	// printf( "optind = [%d]   argc - [%d]\n", optind, argc );

	// No files on the command line
	if ( optind >= argc ) {
		printUsage() ;
		return( EXIT_FAILURE );
	}

	// There's only one file to process
	else if ( optind + 1 == argc ) {
		processFile( argv[optind] );
	}
	
	// At this point, we know we have multiple files
	else {
		for (int i = optind; i < argc; i++) {
			printf( "\n" );
			printf( "File: %s\n", argv[i] );		
		
			processFile( argv[i] );
		}
	}
	
	return( EXIT_SUCCESS );
}
