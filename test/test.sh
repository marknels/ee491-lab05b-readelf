
WORKING_DIR=./tmp

rm -fr $WORKING_DIR
mkdir  $WORKING_DIR

for TEST in test1 test2 test3 test4 test5 test6 test7
do
	head -21 $TEST.out > $WORKING_DIR/reference
	../readelf -h $TEST > $WORKING_DIR/sample
	diff $WORKING_DIR/reference $WORKING_DIR/sample
done

cat all.out > $WORKING_DIR/reference
../readelf -h test1 test2 test3 test4 test5 test6 test7 > $WORKING_DIR/sample
diff $WORKING_DIR/reference $WORKING_DIR/sample

rm -fr $WORKING_DIR
