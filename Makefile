###############################################################################
# University of Hawaii, College of Engineering
# EE 491F - Software Reverse Engineering
# Lab 05c - Readelf
#
# @file    Makefile
# @version 1.0
#
# @author Mark Nelson <marknels@hawaii.edu>
# @brief  Lab 05b - Readelf - EE 491F - Spr 2021
# @date   22 Feb 2021
###############################################################################

CC=gcc
CFLAGS=-Wall -Wno-unused-variable

all: readelf

readelf:  readelf.c
	$(CC) $(CFLAGS) -o $@ $^

test: readelf
	cd test ; ./test.sh
	
clean:
	rm -f *.o readelf
